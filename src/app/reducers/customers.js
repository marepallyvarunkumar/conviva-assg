const initialState = {
    list: [],
    isBeingFetched: false,
    errorInFetching: false,
    errorMessage: ""
};

const customers = (state = initialState, action = {}) => {
    switch (action.type) {
        case 'ADD_CUSTOMERS':
            return {
                ...state,
                list: action.payload,
                isBeingFetched: false
            }
        case 'FETCHING_CUSTOMER_DATA':
            return {
                ...state,
                isBeingFetched: true
            }
        case 'ERROR_FETCHING_CUSTOMER':
            return {
                ...state,
                isBeingFetched: false,
                ...action.payload
            }
        default:
            return state;
    }
};

export default customers;
