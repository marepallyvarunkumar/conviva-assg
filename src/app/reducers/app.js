import { combineReducers } from 'redux';

import labels from './labels';
import customers from './customers';
import addresses from './addresses';

const app = combineReducers({
    labels,
    customers,
    addresses
});

export default app;