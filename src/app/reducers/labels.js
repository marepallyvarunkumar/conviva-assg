const initialState = {
    list: {
        label_customer_list_heading: 'Customers List',
        label_loading: 'Loading...',
        label_age: 'Age',
        label_no_customers: 'Oops! There are no customers listed',
        label_fetching_recent: 'Fetching recent data...',
        label_close: 'Close',
        label_mobile: 'Mobile',
        label_addresses: 'Addresses List',
        
    }
};

const labels = (state = initialState, action = {}) => {
    switch (action.type) {
        case 'SET_INIT_DATA':
            return {
                ...state,
                labelsList: {
                    ...state.labelsList,
                    ...action.payload
                },
                isLabelsFetched: true
            }
        default:
            return state;
    }
};

export default labels;
