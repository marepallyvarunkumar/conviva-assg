/*
    Each customer hash is the object with all details
    
    example object
    customerId: {
        addressList: [],
        isBeingFetched: false,
        errorInFetching: false,
        errorMessage: ""
    }
*/

const defaultCustomerAddressObject = {
    addressList: [],
    isBeingFetched: false,
    errorInFetching: false,
    errorMessage: ""
}

const initialState = {
    customerHash: {}
};

const addresses = (state = initialState, action = {}) => {
    switch (action.type) {
        case 'ADD_CUSTOMER_ADDRESS_OBJECT':
            state.customerHash[action.payload.customerId] = defaultCustomerAddressObject;
            return {
                ...state
            }
        case 'ADD_CUSTOMER_ADDRESSES':
            return {
                ...state,
                customerHash: {
                    ...state.customerHash,
                    [action.payload.customerId]: {
                        addressList: action.payload.addressList,
                        isBeingFetched: false,
                        errorInFetching: false,
                        errorMessage: ""
                    }
                }
            }
        case 'FETCHING_ADDRESSES_DATA':
            return {
                ...state,
                customerHash: {
                    ...state.customerHash,
                    [action.payload.customerId]: {
                        ...state.customerHash[action.payload.customerId],
                        isBeingFetched: true,
                    }
                }
            }
        case 'ERROR_FETCHING_ADDRESSES':
            return {
                ...state,
                customerHash: {
                    ...state.customerHash,
                    [action.payload.customerId]: {
                        ...state.customerHash[action.payload.customerId],
                        isBeingFetched: false,
                        errorInFetching: true,
                        errorMessage: action.payload.errorMessage
                    }
                }
            }
        default:
            return state;
    }
};

export default addresses;
