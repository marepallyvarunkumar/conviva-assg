import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import appReducer from 'reducers/app';
import actionLogMiddleware from 'middleware/actionLogMiddleware';


// Enable this when you wan to use the "Redux - Chrome Extension" to see the flow and
// and use the "composeEnhancers" in place of "compose"

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    appReducer,
    compose(
        applyMiddleware(
            actionLogMiddleware,
            thunkMiddleware
        )
    )
);