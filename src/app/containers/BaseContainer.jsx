import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import BaseComponent from 'components/baseComponent';
import fetchCustomersData from 'actions/fetchCustomersData';


const BaseContainer = (props) => {

	const dispatch = useDispatch();

	const [fetchCustomersCalled, setFetchCustomersCalled] = useState(false);
	useEffect(() => {
		if(!fetchCustomersCalled) {
			dispatch(fetchCustomersData());
			setFetchCustomersCalled(true);
		}
	}, [fetchCustomersCalled, dispatch]);

	return (
		<BaseComponent />
	);
}

export default BaseContainer;
