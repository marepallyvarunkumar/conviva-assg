import applyLabel from './LabelWrapper';
import CSSModules from 'react-css-modules';

const applyWrappers = (WrappedComponent, styles) => {
    return (
        applyLabel(
            CSSModules(
                WrappedComponent,
                styles,
                {
                    allowMultiple: true
                }
            )
        )
    );
}

export default applyWrappers;
