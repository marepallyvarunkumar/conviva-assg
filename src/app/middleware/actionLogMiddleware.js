const actionLogMiddleware = ({ getState }) => {
    return next => action => {
        console.log("Action to be Executed :", action);
        const returnValue = next(action);
        console.log("Successfully Executed :", action.type);
        return returnValue;
    }
};

export default actionLogMiddleware;
