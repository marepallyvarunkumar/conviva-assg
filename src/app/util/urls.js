const baseUrl = 'https://5ea0b01ceea7760016a9200d.mockapi.io/api/v1';

const urls = {
    GET_CUSTOMERS: `${baseUrl}/customers`,
    GET_CUSTOMER_ADDRESSES: `${baseUrl}/customers/{customerId}/addresses`
}

export default urls;