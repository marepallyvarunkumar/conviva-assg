import React, { useState } from 'react';
import styles from './baseComponent.module.sass';
import { useDispatch, useSelector } from 'react-redux';
import applyWrappers from 'wrappers/ComponentWrapper';
import fetchCustomerAddressData from 'actions/fetchCustomerAddressData';
import AddressComponent from 'components/addressComponent';
import Loader from 'assets/three-bar-loader.gif';

const BaseComponent = (props) => {

    const dispatch = useDispatch();

    const myProps = useSelector(state => {
        return {
            customers: state.customers
        }
    });

    const { customers } = myProps;

    const [activeCustomerId, setActiveCustomerId] = useState(null);

    const onClickOfCustomer = (id) => {
        if (id === activeCustomerId) {
            return;
        }
        setActiveCustomerId(id);
        dispatch(fetchCustomerAddressData(id));
    }

    const closeAddressesList = () => {
        setActiveCustomerId(null);
    }

    const renderCustomersList = () => {
        if (customers.isBeingFetched) {
            return (
                <div styleName="full-list-container">
                    <img src={Loader} alt="loading" width="170px" height="70px" />
                    <div>{props.getLabel('label_loading')}</div>
                </div>
            );
        } else if (customers.errorInFetching) {
            return (
                <div styleName="full-list-container">{customers.errorMessage}</div>
            );
        } else if (customers.list.length === 0) {
            return (
                <div styleName="full-list-container">
                    {props.getLabel('label_no_customers')}
                </div>
            );
        }
        return customers.list.map((eachCustomer) => {
            return (
                <div
                    key={`customer-${eachCustomer.id}`}
                    styleName={eachCustomer.id === activeCustomerId ? "customer-container-selected" : "customer-container"}
                    onClick={() => { onClickOfCustomer(eachCustomer.id) }}
                >
                    <div styleName="customer-image">
                        <img src={eachCustomer.avatar} width="30px" height="30px" alt={`customer-${eachCustomer.id}`} />
                    </div>
                    <div styleName="customer-details-container">
                        <div styleName="customer-name">{`${eachCustomer.gender} ${eachCustomer.name}`}</div>
                        <div styleName="customer-details">
                            <div styleName="customer-designation">{eachCustomer.title}</div>
                            <div styleName="customer-age">{`${props.getLabel('label_age')}:${eachCustomer.age}`}</div>
                        </div>
                    </div>
                    <div></div>

                </div>
            );
        })
    }

    const renderCustomerAddresses = () => {
        return (
            <AddressComponent
                activeCustomerId={activeCustomerId}
                closeComponent={closeAddressesList}
            />
        );
    }

    return (
        <div styleName="base-container">
            <div styleName="customers-list">
                <div styleName="list-heading">{props.getLabel('label_customer_list_heading')}</div>
                <div styleName="customers-list-container">
                    {renderCustomersList()}
                </div>
            </div>
            {
                activeCustomerId != null
                    ? renderCustomerAddresses()
                    : null
            }
        </div>
    );
}

export default applyWrappers(BaseComponent, styles);
