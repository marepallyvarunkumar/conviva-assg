import React, { useState } from 'react';
import styles from './closeComponent.module.sass';
import applyWrappers from 'wrappers/ComponentWrapper';
import Cancel from 'assets/cancel.png';

const CloseComponent = (props) => {

    const [onHover, setOnHover] = useState(false);

    return (
        <div styleName="close-container">
            <div styleName="close-component"
                onClick={props.clickFunction}
                onMouseEnter={() => { setOnHover(true); }}
                onMouseLeave={() => { setOnHover(false); }}
            >
                <div styleName="close-icon">
                    <img src={Cancel} alt="close" width="100%" height="100%" />
                </div>
                <div styleName={onHover ? "close-text-underline" : "close-text"}>Close</div>
            </div>
        </div>
    );
}

export default applyWrappers(CloseComponent, styles);
