import React from 'react';
import styles from './addressComponent.module.sass';
import { useSelector } from 'react-redux';
import applyWrappers from 'wrappers/ComponentWrapper';
import CloseComponent from 'components/closeComponent';
import Loader from 'assets/loader.gif';

const AddressComponent = (props) => {

    const customerId = props.activeCustomerId;

    const myProps = useSelector(state => {
        return {
            addresses: state.addresses
        }
    });

    const customerDetails = myProps.addresses.customerHash[customerId];

    if (customerDetails === undefined) {
        return (
            <div>
                No Data avilable for the customer
            </div>
        );
    }

    const getFetchingDataDiv = () => {
        if (customerDetails.isBeingFetched) {
            return (
                <div styleName="header-loading">
                    <div styleName="header-loading-image">
                        <img src={Loader} alt="loading" width="100%" height="100%" />
                    </div>
                    <div styleName="header-loading-text">{props.getLabel('label_fetching_recent')}</div>
                </div>
            );
        }
        return null;
    }

    const renderAddresses = () => {
        if (customerDetails.addressList.length === 0 && !customerDetails.isBeingFetched) {
            return (
                <div>
                    No addresses found for this user
                </div>
            );
        }
        return customerDetails.addressList.map((eachAddress) => {
            return (
                <div key={`address-${eachAddress.id}`} styleName="each-address-container">
                    <div styleName="text-italic">{eachAddress.street},</div>
                    <div styleName="text-italic">{eachAddress.city},</div>
                    <div styleName="text-italic">{eachAddress.state},</div>
                    <div styleName="text-italic">{eachAddress.country} - {eachAddress.zipcode}</div>
                    <div styleName="line"></div>
                    <div styleName="text">{`${props.getLabel('label_mobile')}: ${eachAddress.phone}`}</div>
                </div>
            );
        });
    }

    const renderErrorMessage = () => {
        if (customerDetails.errorInFetching) {
            return (
                <div>
                    {customerDetails.errorMessage}
                </div>
            );
        }
    }

    return (
        <div styleName="customers-address-container">
            <div styleName="address-container">
                <CloseComponent clickFunction={props.closeComponent} />
                <div styleName="header">
                    <div styleName="header-text">{props.getLabel('label_addresses')}</div>
                    {getFetchingDataDiv()}
                </div>
                <div styleName="address-list-container">
                    {renderAddresses()}
                </div>
                {renderErrorMessage()}
            </div>
        </div>
    );
}

export default applyWrappers(AddressComponent, styles);
