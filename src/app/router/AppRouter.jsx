import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import BaseContainer from './../containers/BaseContainer';

const AppRouter = () => {
    return (
        <HashRouter>
            <Route path="/" component={BaseContainer} />
        </HashRouter>
    );
};

export default AppRouter;
