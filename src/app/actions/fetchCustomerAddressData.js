import myFetch from 'util/myFetch';
import urls from 'util/urls';
import {
    addCustomerAddressObject,
    addAddresses,
    fetchingAddresses,
    errorInFetchingAddresses,
} from 'actions/creators';

const fetchCustomerAddressData = (customerId) => {

    return (dispatch, getStore) => {

        const store = getStore();
        const addresses = store.addresses;

        if (!(customerId in addresses.customerHash)) {
            dispatch(addCustomerAddressObject(customerId));
        }
        
        dispatch(fetchingAddresses(customerId));

        myFetch(
            urls.GET_CUSTOMER_ADDRESSES.replace('{customerId}', customerId)
        ).then((response) => {
            dispatch(addAddresses(customerId, response));
        }).catch((error) => {
            dispatch(errorInFetchingAddresses(customerId, error.message));
        });
    }
}

export default fetchCustomerAddressData;