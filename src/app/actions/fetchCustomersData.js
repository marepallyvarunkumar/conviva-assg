import myFetch from 'util/myFetch';
import urls from 'util/urls';
import {
    addCustomers,
    fetchingCustomers,
    errorInCustomersFetch
} from 'actions/creators';

const fetchCustomerData = () => {

    return (dispatch, { getStore }) => {

        dispatch(fetchingCustomers());

        myFetch(
            urls.GET_CUSTOMERS
        ).then((response) => {
            dispatch(addCustomers(response));
        }).catch((error) => {
            dispatch(errorInCustomersFetch(error.message));
        });
    }
}

export default fetchCustomerData;