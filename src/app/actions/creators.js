const addCustomers = (customerList) => {
    return {
        type: 'ADD_CUSTOMERS',
        payload: customerList
    };
}

const fetchingCustomers = () => {
    return {
        type: 'FETCHING_CUSTOMER_DATA'
    }
}

const errorInCustomersFetch = (message) => {
    return {
        type: 'ERROR_FETCHING_CUSTOMER',
        payload: {
            errorInFetching: true,
            errorMessage: message
        }
    }
}

const addCustomerAddressObject = (customerId) => {
    return {
        type: 'ADD_CUSTOMER_ADDRESS_OBJECT',
        payload: {
            customerId
        }
    }
}

const addAddresses = (customerId, addressList) => {
    return {
        type: 'ADD_CUSTOMER_ADDRESSES',
        payload: {
            customerId,
            addressList
        }
    };
}

const fetchingAddresses = (customerId) => {
    return {
        type: 'FETCHING_ADDRESSES_DATA',
        payload: {
            customerId
        }
    };
}

const errorInFetchingAddresses = (customerId, message) => {
    return {
        type: 'ERROR_FETCHING_ADDRESSES',
        payload: {
            customerId,
            errorInFetching: true,
            errorMessage: message
        }
    };
}

export {
    //Customer Reducer Action Creators
    addCustomers,
    fetchingCustomers,
    errorInCustomersFetch,
    // Address Reducer Action Creators
    addCustomerAddressObject,
    addAddresses,
    fetchingAddresses,
    errorInFetchingAddresses,
};